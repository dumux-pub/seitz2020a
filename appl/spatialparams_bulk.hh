// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EmbeddedTests
 * \brief Definition of the spatial parameters for the matrix and fracture problem.
 */

#ifndef DUMUX_MATRIX_TEST_SPATIAL_PARAMS_HH
#define DUMUX_MATRIX_TEST_SPATIAL_PARAMS_HH

#include <cmath>
#include <dumux/material/spatialparams/fv1p.hh>
#include "permeabilitypowerlaw.hh"
#include <dumux/discretization/evalsolution.hh>

namespace Dumux {

/*!
 * \ingroup EmbeddedTests
 * \brief Definition of the spatial parameters for the matrix and fracture problem.
 */
template<class GridGeometry, class Scalar, class FluidSystem, class FluidState>
class MatrixSpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar, MatrixSpatialParams<GridGeometry, Scalar, FluidSystem, FluidState>>
{
    using ThisType = MatrixSpatialParams<GridGeometry, Scalar, FluidSystem, FluidState>;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar, ThisType>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolume = typename GridGeometry::SubControlVolume;

public:
    // export permeability type
    using PermeabilityType = Scalar;

    MatrixSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                                const std::string& paramGroup = "")
    : ParentType(gridGeometry)
    {
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "Matrix.SpatialParams.Permeability");
    }

    /*!
     * \brief Defines the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The element
     * \param scv The sub control volume
     * \param elemSol The element solution vector
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        Scalar referencePermeability = 8.8e-12;
//         Scalar referencePermeability = 8.8e-11;

//         auto priVars = evalSolution(element, element.geometry(), elemSol, scv.center());

//         Scalar sumPrecipitates = 0.0;
//         for (unsigned int solidPhaseIdx = 0; solidPhaseIdx < 2; ++solidPhaseIdx)
//             sumPrecipitates += priVars[/*numComp*/1 + solidPhaseIdx];
// //
// //         using std::max;
//         const auto poro = 1 - sumPrecipitates;
//
//         return permLaw_.evaluatePermeability(referencePermeability, 0.89 /*refPoro*/, poro)/** kFactor*/;
        return referencePermeability;
    }


private:
    Scalar permeability_;
    Scalar porosity_;
   PermeabilityPowerLaw<PermeabilityType> permLaw_;
};

} // end namespace Dumux

#endif
