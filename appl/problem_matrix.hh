// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \ingroup EmbeddedTests
 * \brief The matrix problem.
 */

#ifndef DUMUX_MATRIX_ROBLEM_HH
#define DUMUX_MATRIX_ROBLEM_HH

#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/1pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidmatrixinteractions/1p/thermalconductivityaverage.hh>
#include <dumux/material/components/cao2h2.hh>
//  #include <dumux/material/components/cao.hh>
#include <dumux/material/solidsystems/compositionalsolidphase.hh>

#include <dumux/material/components/h2o.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

#include "spatialparams_bulk.hh"
#include "thermochemreaction.hh"
#include "mycao.hh"

namespace Dumux {

template <class TypeTag>
class MatrixProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct Matrix { using InheritsFrom = std::tuple<OnePNCMinNI, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Matrix> { using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::Matrix> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::Matrix> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::Matrix> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Matrix> { using type = MatrixProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Matrix>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePGas<Scalar, Components::H2O<Scalar> >;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::Matrix>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::MyCaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<Scalar, ComponentOne, ComponentTwo>;
};
// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Matrix>
{
    using type = MatrixSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>,
                                             GetPropType<TypeTag, Properties::Scalar>,
                                             GetPropType<TypeTag, Properties::FluidSystem>,
                                             GetPropType<TypeTag, Properties::FluidState>>;
};
// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Matrix> { static constexpr bool value = true; };
} // end namespace Properties


/*!
 * \ingroup EmbeddedTests
 * \brief The matrix problem.
 */
template <class TypeTag>
class MatrixProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SourceValues = GetPropType<TypeTag, Properties::NumEqVector>;
    using ThermalConductivityModel = GetPropType<TypeTag, Properties::ThermalConductivityModel>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ReactionRate = ThermoChemReactionPoro;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    enum {
        // world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum
    {
        // Indices of the primary variables
        pressureIdx = Indices::pressureIdx, //gas-phase pressure

        H2OIdx = FluidSystem::comp0Idx,
        CaOIdx = FluidSystem::numComponents,
        CaO2H2Idx = FluidSystem::numComponents+1,

        // Equation Indices
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase Indices
        phaseIdx = FluidSystem::phase0Idx,
        cPhaseIdx = SolidSystem::comp0Idx,

        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    MatrixProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                  std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                  std::shared_ptr<CouplingManager> couplingManager,
                  const std::string& paramGroup = "Matrix")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManager_(couplingManager)
    {
        // read parameters from input file
        name_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        deltaYBulk_ = getParam<GlobalPosition>("Matrix.Grid.UpperRight")[1] / getParam<GlobalPosition>("Matrix.Grid.Cells")[1];
        deltaYFrac_ = getParam<Scalar>("Fracture.SpatialParams.Aperture");

        bulkVolume_ = getParam<GlobalPosition>("Matrix.Grid.UpperRight")[0] * getParam<GlobalPosition>("Matrix.Grid.UpperRight")[1];

        //loss factors
        kL_ = getParam<Scalar>("Optimization.HeatLossParam");
        kT_ = getParam<Scalar>("Optimization.HeatTransferParam");
        a_ = getParam<Scalar>("Optimization.SecondOrderParam1");
        b_ = getParam<Scalar>("Optimization.SecondOrderParam2");
        c_ = getParam<Scalar>("Optimization.SecondOrderParam3");

        unsigned int codim = GetPropType<TypeTag, Properties::FVGridGeometry>::discMethod == DiscretizationMethod::box ? dim : 0;
        permeability_.resize(gridGeometry->gridView().size(codim));
        porosity_.resize(gridGeometry->gridView().size(codim));
        reactionRate_.resize(gridGeometry->gridView().size(codim));
        tEq_.resize(gridGeometry->gridView().size(codim));

        tInit_ = getParam<Scalar>("Matrix.InitialTemperature");
        outputFile_.open(getParamFromGroup<std::string>(this->paramGroup(), "Vtk.OutputName")+".csv", std::ios::out);
        outputFile_ << "time , T1 , T3 , T7 , conversion "  << std::endl;
        outputFile_ << "0 , "<< tInit_ <<" , " << tInit_ <<" , " << tInit_ <<" , 0 "  << std::endl; // initial conditions
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain [K].
     */
    Scalar temperature() const
    { return 733.15; } // in [K]

    /*!
     * \brief Returns the time in [s].
     */
    void setTimeStep(Scalar t)
    { timeStep_ = t; }

    void setTime(Scalar t)
    { time_ = t + timeStep_; } // the time of the finished timesteps plus the current timestep size

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The position for which the bc type should be evaluated
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);
        const auto& globalPos = scvf.ipGlobal();
        const auto& scv = fvGeometry.scv(scvf.insideScvIdx());
        Scalar dirichletPressure = getParam<Scalar>("Matrix.DirichletPressure");
        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];

        if (globalPos[1] < eps_){

            const Scalar K = elemVolVars[scv].permeability();

            // construct the element solution
            const auto elemSol = [&]()
            {
                auto sol = elementSolution(element, elemVolVars, fvGeometry);
                return sol;
            }();
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();;
                const Scalar scvCenterPresureSol = elemSol[0][Indices::pressureIdx];
                auto grad = ipGlobal- scvCenter;
                grad /= grad.two_norm2();  //see dune FieldVector; quadrierte Normierung
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();

            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0  * K;
            tpfaFlux *=  volVars.molarDensity() * volVars.mobility();

            flux[Indices::conti0EqIdx] = tpfaFlux;

            Scalar tpfaFluxMass = gradient * scvf.unitOuterNormal();
            tpfaFluxMass *= -1.0  * K;
            tpfaFluxMass *=  volVars.density()  * volVars.mobility();

            // for outflux use values for gas conditions of the boundary cell
            if(tpfaFluxMass >= 0.0){
                FluidState fluidStateBoundary;
                fluidStateBoundary.setPressure(0, dirichletPressure);                 fluidStateBoundary.setTemperature(tempAtPos(globalPos));

//              // heat loss
                Scalar qLoss = (volVars.temperature()-fluidStateBoundary.temperature())*kL_;
                flux[Indices::energyEqIdx] = tpfaFluxMass * (FluidSystem::enthalpy(volVars.fluidState(), phaseIdx)) + qLoss;

            }
            // for influx use values for gas conditions of the initial conditions
            if(tpfaFluxMass < 0.0){
                FluidState fluidStateBoundary;
                fluidStateBoundary.setPressure(0, dirichletPressure);                 fluidStateBoundary.setTemperature(tempAtPos(globalPos));

//              // heat loss
                Scalar qLoss = (volVars.temperature()-fluidStateBoundary.temperature())*kL_; //
                flux[Indices::energyEqIdx] = tpfaFluxMass * (FluidSystem::enthalpy(fluidStateBoundary, phaseIdx)) + qLoss;
            }
        }

        // heat loss on left and right side without water flux
        if(globalPos[0] < 0.0 + eps_){
            flux[Indices::energyEqIdx] = (volVars.temperature()-tempAtPos(globalPos))*kL_;
        }
        if(globalPos[0] > 1.6 - eps_){
            flux[Indices::energyEqIdx]= (volVars.temperature()-tempAtPos(globalPos))*kL_;
        }

        return flux;
    }

    // boundary temperature
    Scalar tempAtPos(const GlobalPosition &globalPos) const
    {
        Scalar T = 0.0;
        T = a_ * globalPos[0] * globalPos[0] + b_* globalPos[0] + c_;
        return T;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume in units of \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilated per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {

        ////// reaction source
        NumEqVector source(0.0);
        const auto& volVars = elemVolVars[scv];

        Scalar qMass = rrate_.thermoChemReactionSimple(volVars);
        Scalar  qMole =  qMass/FluidSystem::molarMass(H2OIdx)*(1-volVars.porosity());

        // make sure not more solid reacts than present
        // for CaO
        if (-qMole*timeStep_ + volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx) < 0 + eps_)
        {
            qMole = volVars.solidVolumeFraction(cPhaseIdx)* volVars.solidComponentMolarDensity(cPhaseIdx)/timeStep_;
        }

        source[conti0EqIdx+CaO2H2Idx] = qMole;
        source[conti0EqIdx+CaOIdx] = - qMole;
        source[conti0EqIdx+H2OIdx] = - qMole;

        Scalar deltaH = 108e3; // J/mol
        source[energyEqIdx] = qMole * (deltaH - (volVars.porosity()/(1-volVars.porosity()))*(volVars.pressure(phaseIdx)/volVars.molarDensity(phaseIdx)));

        return source;
    }

    /*!
     * \brief Applies a vector of point sources which are possibly solution dependent.
     *
     * \param pointSources A vector of Dumux::PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().bulkPointSources(); }

    /*!
     * \brief Evaluates the point sources (added by addPointSources)
     *        for all phases within a given sub-control volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param source A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilated in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        SourceValues sourceValues(0.0);

        // retreive temperatures
        // attention to get the right privar indices:
        //   - fracture: pressureIdx = 0 ; temperatureIdx = 1
        //   - matrix: pressureIdx = 0; CaOIdx = 1; CaO2H2Idx = 2; temperatureIdx = 3
        const Scalar temperature2D = this->couplingManager().bulkPriVars(source.id())[Indices::temperatureIdx];
        const Scalar temperature1D = this->couplingManager().lowDimPriVars(source.id())[Indices::temperatureIdx-2];

        // calculate the source
//         const Scalar distance =
        const Scalar meanDistance = 0.5* deltaYBulk_ + 0.5*deltaYFrac_;
//         std::cout << "meanDistance " << meanDistance << "\n";
        const auto& volVars = elemVolVars[scv];

        const auto lambdaPm = ThermalConductivityModel::effectiveThermalConductivity(volVars);
        Scalar alphaFf = kT_ ; // [W/m^2K]

        Scalar fluxPm = temperature1D * ( lambdaPm * alphaFf ) / (lambdaPm + meanDistance*alphaFf)- temperature2D * (lambdaPm/meanDistance - lambdaPm*lambdaPm/(lambdaPm*meanDistance + alphaFf*meanDistance*meanDistance));

        sourceValues[Indices::temperatureIdx] = 1.0*fluxPm;
        sourceValues *= source.quadratureWeight()*source.integrationElement();
        source = sourceValues;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        Scalar pInit = getParam<Scalar>("Matrix.InitialPressure");
        Scalar caOInit = getParam<Scalar>("Matrix.InitialCaO");
        Scalar caOH2Init = getParam<Scalar>("Matrix.InitialCaOH2");

        PrimaryVariables values;
        values[Indices::pressureIdx] = pInit;
        values[Indices::temperatureIdx] = tInit_;
        values[CaOIdx] = caOInit;
        values[CaO2H2Idx] = caOH2Init;

        return values;
    }

    //! Called after every time step
    //! Output the total global exchange term
    void computeSourceIntegral(const SolutionVector& sol, const GridVariables& gridVars)
    {
        NumEqVector source(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                source += pointSources;
            }
        }

        std::cout << "Global integrated source (3D): " << source << '\n';
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

     /*!
     * \brief Returns the reaction rate.
     */
    const std::vector<Scalar>& getRRate()
    {
        return reactionRate_;
    }
     /*!
     * \brief Returns the equilibrium temperature.
     */
    const std::vector<Scalar>& getTeq()
    {
        return tEq_;
    }

   /*!
     * \brief Returns the permeability.
     */
    const std::vector<Scalar>& getPerm()
    {
        return permeability_;
    }

   /*!
     * \brief Returns the porosity.
     */
    const std::vector<Scalar>& getPoro()
    {
        return porosity_;
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter.
     *
     * Function is called by the output module on every write.
     */
    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                permeability_[dofIdxGlobal] = this->spatialParams().permeability(element, scv, elemSol);
                porosity_[dofIdxGlobal] = volVars.porosity();
                reactionRate_[dofIdxGlobal] = (1-volVars.porosity())*rrate_.thermoChemReactionSimple(volVars);
                tEq_[dofIdxGlobal] = -12845/(log(volVars.pressure(0)*1.0e-5) - 16.508);
            }
        }
    }

    //! Called after every time step
    //! Output the total flux balances and conversion
    void evaluate2dBalances(NumEqVector& neumannOutFlux, const SolutionVector& sol, const GridVariables& gridVars)
    {
        Scalar outMass = 0.0;
        Scalar outNeumannEnthaply = 0.0;
        Scalar T1 = 0.0;
        Scalar T3 = 0.0;
        Scalar T7 = 0.0;
        Scalar sumCaO = 0.0;
        Scalar sumCaOH2 = 0.0;
        Scalar volume = bulkVolume_ ;
        Scalar maxCaH =  0.227; //see input file TODO retreive from input file

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                Scalar cellCaO = elemVolVars[scv].solidVolumeFraction(CaOIdx-1);
                cellCaO *= scv.volume()*elemVolVars[scv].extrusionFactor();
                sumCaO += cellCaO;
                Scalar cellCaH = elemVolVars[scv].solidVolumeFraction(CaOIdx);
                cellCaH *= scv.volume()*elemVolVars[scv].extrusionFactor();
                sumCaOH2 += cellCaH;

//                 //805 45 cells
                const auto& ipGlobal= scv.dofPosition();
                if(ipGlobal[0] > 0.2 - 1.0e-3 && ipGlobal[0] < 0.2 + 1.0e-4 &&
                    ipGlobal[1] > 0.005 - 1.0e-6 && ipGlobal[1] < 0.005 + 1.0e-6)
                    T1 = elemVolVars[scv].temperature();
                if(ipGlobal[0] > 0.6 - 1.0e-3 && ipGlobal[0] < 0.6 + 1.0e-4 &&
                    ipGlobal[1] > 0.005 - 1.0e-6 && ipGlobal[1] < 0.005 + 1.0e-6)
                    T3 = elemVolVars[scv].temperature();
                if(ipGlobal[0] > 1.4 - 5.0e-4 && ipGlobal[0] < 1.4 + 5.0e-4 &&
                    ipGlobal[1] > 0.005 - 1.0e-6 && ipGlobal[1] < 0.005 + 1.0e-6)
                    T7 = elemVolVars[scv].temperature();
            }
        }

        // print to outfile
        outputFile_ << time_<< " , " << T1 << " , " << T3 << " , " << T7 << " , " << (sumCaOH2/volume)/maxCaH <<  std::endl;
    }


private:

    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;

    std::shared_ptr<CouplingManager> couplingManager_;
    ReactionRate rrate_;
    std::vector<double> permeability_;
    std::vector<double> porosity_;
    std::vector<double> reactionRate_;
    std::vector<double> tEq_;
    Scalar deltaYBulk_;
    Scalar deltaYFrac_;
    Scalar bulkVolume_;
    std::ofstream outputFile_;
    Scalar timeStep_;
    Scalar time_;
    Scalar tInit_;

    Scalar kL_;
    Scalar kT_;
    Scalar a_;
    Scalar b_;
    Scalar c_;
};

} // end namespace Dumux

#endif
