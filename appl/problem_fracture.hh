// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup EmbeddedTests
 * \brief A fracture problem.
 */

#ifndef DUMUX_FRACTURE_PROBLEM_HH
#define DUMUX_FRACTURE_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>
#include <dumux/discretization/elementsolution.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/components/n2.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/fluidsystems/1pgas.hh>

#include "spatialparams_fracture.hh"

namespace Dumux {
// forward declaration
template <class TypeTag> class FractureProblem;

namespace Properties {

// Create new type tags
namespace TTag {
struct Fracture { using InheritsFrom = std::tuple<OnePNI, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Fracture> { using type = Dune::FoamGrid<1, 2>; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::Fracture> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::Fracture> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::Fracture> { static constexpr bool value = false; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Fracture> { using type = FractureProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Fracture>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePGas<Scalar, Components::N2<Scalar> >;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Fracture>
{
    using type = FractureSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>,
                                             GetPropType<TypeTag, Properties::Scalar>>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Fracture> { static constexpr bool value = false; };
} // end namespace Properties

/*!
 * \ingroup EmbeddedTests
 * \brief Exact solution 1D-3D.
 */
template <class TypeTag>
class FractureProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PointSource = GetPropType<TypeTag, Properties::PointSource>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SourceValues = GetPropType<TypeTag, Properties::NumEqVector>;
    using ThermalConductivityModel = GetPropType<TypeTag, Properties::ThermalConductivityModel>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using H2O = Components::H2O<Scalar>;

    enum
    {
        // Phase Indices
        phaseIdx = FluidSystem::phase0Idx,
    };

public:
    FractureProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup = "Fracture")
    : ParentType(gridGeometry, spatialParams, paramGroup)
    , couplingManager_(couplingManager)
    {
        // read parameters from input file
        name_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        deltaYBulk_ = getParam<std::string>("Matrix.Grid.UpperRight")[1] / getParam<std::string>("Matrix.Grid.Cells")[1];
        deltaYFrac_ = getParam<Scalar>("Fracture.SpatialParams.Aperture");
        kT_ = getParam<Scalar>("Optimization.HeatTransferParam");
    }

    /*!
     * \brief Returns how much the domain is extruded at a given sub-control volume.
     *
     * The extrusion factor here extrudes the 1d line to a circular tube with
     * cross-section area pi*r^2.
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element &element,
                           const SubControlVolume &scv,
                           const ElementSolution& elemSol) const
    {
        static const Scalar aperture = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Aperture");
        return aperture;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 533.15; }

    /*!
     * \brief Returns the time  in [s].
     *
     */
    void setTime(Scalar t)
    { time_ = t; }

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

   /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        if (globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_){

            values[Indices::pressureIdx] = 5.0e5;
            values[Indices::temperatureIdx] = 533.15;
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     *
     * E.g. for the mass balance that would be the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto globalPos = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        Scalar inFluxMass  =  getParam<Scalar>("Fracture.InFlux");
        Scalar inletTemperature  =  getParam<Scalar>("Fracture.TemperatureInlet");

        if(globalPos[0] < eps_)
        {
            values[Indices::conti0EqIdx] = - inFluxMass; //  [kg/m^2s] //6* 4.31/6* 5.74/6*7.18
            values[Indices::energyEqIdx] = values[Indices::conti0EqIdx]*FluidSystem::enthalpy(inletTemperature, volVars.pressure());
        }

        if(globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_)
        {
            const Scalar dirichletPressure = 5.0e5;
            const auto elemSol = [&]()
            {
                auto sol = elementSolution(element, elemVolVars, fvGeometry);
                return sol;
            }();
            // evaluate the gradient
            const auto gradient = [&]()->GlobalPosition
            {
                const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();;
                const Scalar scvCenterPresureSol = elemSol[0][Indices::pressureIdx];
                auto grad = globalPos- scvCenter;
                grad /= grad.two_norm2();  //see dune FieldVector;
                grad *= (dirichletPressure - scvCenterPresureSol);
                return grad;
            }();
            Scalar tpfaFlux = gradient * scvf.unitOuterNormal();
            tpfaFlux *= -1.0 * volVars.permeability()* volVars.mobility(phaseIdx)*
            volVars.density(phaseIdx);
            values[Indices::pressureIdx] = tpfaFlux;
            values[Indices::temperatureIdx] = values[Indices::pressureIdx]* (FluidSystem::enthalpy(volVars.temperature(), volVars.pressure()));
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

     /*!
     * \brief Applies a vector of point sources which are possibly solution dependent.
     *
     * \param pointSources A vector of PointSource s that contain
              source values for all phases and space positions.
     *
     * For this method, the \a values method of the point source
     * has to return the absolute mass rate in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void addPointSources(std::vector<PointSource>& pointSources) const
    { pointSources = this->couplingManager().lowDimPointSources(); }

    /*!
     * \brief Evaluates the point sources (added by addPointSources)
     *        for all phases within a given sub-control volume.
     *
     * This is the method for the case where the point source is
     * solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param source A single point source
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub-control volume within the element
     *
     * For this method, the \a values() method of the point sources returns
     * the absolute rate mass generated or annihilated in kg/s. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    template<class ElementVolumeVariables>
    void pointSource(PointSource& source,
                     const Element &element,
                     const FVElementGeometry& fvGeometry,
                     const ElementVolumeVariables& elemVolVars,
                     const SubControlVolume &scv) const
    {
        SourceValues sourceValues(0.0);


        // retreive priVars of the two subproblems
        // attention to get the right privar indices:
        //   - fracture: pressureIdx = 0 ; temperatureIdx = 1
        //   - matrix: pressureIdx = 0; CaOIdx = 1; CaO2H2Idx = 2; temperatureIdx = 3
        const Scalar temperature2D = this->couplingManager().bulkPriVars(source.id())[Indices::temperatureIdx+2];
        const Scalar pressure2D = this->couplingManager().bulkPriVars(source.id())[Indices::pressureIdx];
        const Scalar phiCaO = this->couplingManager().bulkPriVars(source.id())[Indices::conti0EqIdx+1];
        const Scalar phiCaOH2 = this->couplingManager().bulkPriVars(source.id())[Indices::conti0EqIdx+2];

        const Scalar temperature1D = this->couplingManager().lowDimPriVars(source.id())[Indices::temperatureIdx];

        // compute source at every integration point
        Scalar porosity = 1- phiCaO-phiCaOH2;
        Scalar solidThermalConductivity = getParamFromGroup<Scalar>("Component", "SolidThermalConductivity");
        Scalar vaporThermalConductivity = H2O::gasThermalConductivity(temperature2D, pressure2D);

        Scalar lambdaPm = porosity * vaporThermalConductivity + (1- porosity)*solidThermalConductivity;

        const Scalar meanDistance = 0.5* deltaYBulk_ + 0.5*deltaYFrac_;

        Scalar alphaFf = kT_ ; // [W/m^2K]

        Scalar fluxPm = temperature1D * ( lambdaPm * alphaFf ) / (lambdaPm + meanDistance*alphaFf)- temperature2D * (lambdaPm/meanDistance - lambdaPm*lambdaPm/(lambdaPm*meanDistance + alphaFf*meanDistance*meanDistance));

        sourceValues[Indices::temperatureIdx] = - 2.0*fluxPm;
        sourceValues *= source.quadratureWeight()*source.integrationElement();
        source = sourceValues;
    }

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        Scalar pInit  =  getParam<Scalar>("Fracture.InitialPressure");
        Scalar tInit  =  getParam<Scalar>("Fracture.InitialTemperature");

        PrimaryVariables values;
        values[Indices::pressureIdx] = pInit; //alternativ: 1.1e5
        values[Indices::temperatureIdx] = tInit;
        return values;
    }

    // \}

    //! Called after every time step
    //! Output the total global exchange term
    void computeSourceIntegral(const SolutionVector& sol, const GridVariables& gridVars)
    {
        NumEqVector source(0.0);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVars.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, sol);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto pointSources = this->scvPointSources(element, fvGeometry, elemVolVars, scv);
                pointSources *= scv.volume()*elemVolVars[scv].extrusionFactor();
                source += pointSources;
            }
        }

        std::cout << "Global integrated source (1D): " << source << '\n';
    }


    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:

    static constexpr Scalar eps_ = 1.5e-7;
    std::string name_;

    std::shared_ptr<CouplingManager> couplingManager_;
    std::ofstream outputFile_;
    Scalar deltaYBulk_;
    Scalar deltaYFrac_;
    Scalar time_;
    Scalar kT_;
};

} // end namespace Dumux

#endif
