[TimeLoop]
DtInitial = 0.001                              # [s]
TEnd = 6000                               # [s]
EpisodeLength = 60                         # [s]
MaxTimeStepSize = 20                       # [s]

[Fracture.Grid]
LowerLeft = 0.0 0.01
UpperRight = 1.6 0.01
Cells = 805

[Matrix.Grid]
LowerLeft = 0 0
UpperRight = 1.6 0.01
Cells = 805 45

[Problem]
EnableGravity = false

[Matrix]
SpatialParams.Permeability = 8.53e-12      # [m^2]
Problem.Name = 2d
SpatialParams.Porosity = 0.773             # [-]
DirichletPressure = 270e3                  # [Pa] lower boundary
DirichletTemperature = 773.15              # [K] lower boundary
InitialPressure = 270e3                    # [Pa]
InitialTemperature = 763.15                # [K]
InitialCaO = 0.1113                          # [-] solid volume fraction: 2.4 kg Ca(OH)2 = 1.816 kg CaO, total volume = 4.8 litres
InitialCaOH2 = 0.0                         # [-] solid volume fraction

[Fracture]
Problem.Name = 1d
SpatialParams.Permeability = 8.3e-8        # [m^2] Hagen Poiseuille between two plates: 1/12 h^2
SpatialParams.Aperture = 0.001             # [m]
InFlux = 47.22                             # [kg/m^2s] left boundary
DirichletPressure = 5.0e5                  # [Pa]
InitialPressure = 5.0e5                    # [Pa]
InitialTemperature = 773.15                # [K]
TemperatureInlet = 773.15                  # [K]

[Optimization]
HeatTransferParam = 154.0                  # range: 150 - 350
HeatLossParam = 30.5                       # range: 0 - 350; most probable between 10 - 50
SecondOrderParam1 = -12.3                  # range: -15 - 15;
SecondOrderParam2 = 15.3                   # range: -20 - 20;
SecondOrderParam3 = 742                    # range: 740 - 780; most probable between 755 - 773
ReactionParam1 = 7.47                      # range: 0.1 - 20;
ReactionParam2 = 2                         # range: 0 - 3


[LinearSolver]
MaxIterations = 2000
ResidualReduction = 1e-24

[Vtk]
OutputName = case_20
AddVelocity = 1

[Component]
SolidThermalConductivity = 0.4
SolidHeatCapacity = 790
SolidDensity = 2700

[Newton]
MaxRelativeShift = 1e-3
MaxSteps = 10

####
#data from figure 4 in Risthaus2020
# references:
# T1 at (0.005 / 0.2)
# T3 at (0.005 / 0.6)
# T7 at (0.005 / 1.4)
# global conversion
