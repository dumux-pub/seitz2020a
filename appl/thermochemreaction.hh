// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePNCMinTests
 * \brief Class for the evaluation of the reaction rate of Calciumoxide to Halciumhydroxide with induced porosity changes
 *
 * It contains simple and advanced reaction kinetics according to Nagel et al. (2014).
 */
#ifndef DUMUX_THERMOCHEM_REACTION_PORO_HH
#define DUMUX_THERMOCHEM_REACTION_PORO_HH

namespace Dumux {

/*!
 * \ingroup OnePNCMinTests
 * \brief Class for the evaluation of the reaction rate of Calciumoxide to Calciumhydroxide
 *
 * It contains simple and advanced reaction kinetics according to Nagel et al. (2014).
 */
class ThermoChemReactionPoro {

public:

    /*!
     * \brief evaluates the simple chemical reaction kinetics (see Nagel et al. 2014)
     */
    template<class VolumeVariables>
    typename VolumeVariables::PrimaryVariables::value_type
    thermoChemReactionSimple(const VolumeVariables &volVars) const
    {
        using FluidSystem = typename VolumeVariables::FluidSystem;
        using SolidSystem = typename VolumeVariables::SolidSystem;

        static constexpr auto H2OIdx = FluidSystem::comp0Idx;
        static constexpr int cPhaseIdx = SolidSystem::comp0Idx;
        static constexpr int hPhaseIdx = SolidSystem::comp1Idx;

        using Scalar = typename VolumeVariables::PrimaryVariables::value_type;

        // calculate the equilibrium temperature Teq
        Scalar T= volVars.temperature();
        Scalar Teq = 0;

        Scalar moleFractionVapor = 1e-3;

        if(volVars.moleFraction(0, H2OIdx) > 1e-3)
            moleFractionVapor = volVars.moleFraction(0, H2OIdx);

        if(volVars.moleFraction(0, H2OIdx) >= 1.0) moleFractionVapor = 1.0;

        //// equilibrium temperature according to Schaube2013
//         Scalar pV = volVars.pressure(0) *moleFractionVapor;
//         Scalar vaporPressure = pV*1.0e-5;
//         Scalar pFactor = log(vaporPressure);
//
//         Teq = -12845;
//         Teq /= (pFactor - 16.508); //the equilibrium temperature

        //// equilibrium temperature according to risthaus2020
        Scalar pV = volVars.pressure(0) *moleFractionVapor;
        Scalar vaporPressure = pV*1.0e-5;
        Scalar pFactor = log(vaporPressure);

        Teq = -11375;
        Teq /= (pFactor - 14.574); //the equilibrium temperature


        Scalar realSolidDensityAverage = (volVars.solidVolumeFraction(hPhaseIdx)*volVars.solidComponentDensity(hPhaseIdx)
                                        + volVars.solidVolumeFraction(cPhaseIdx)*volVars.solidComponentDensity(cPhaseIdx))
                                        / (volVars.solidVolumeFraction(hPhaseIdx)
                                        + volVars.solidVolumeFraction(cPhaseIdx));

        if(realSolidDensityAverage >= volVars.solidComponentDensity(cPhaseIdx))
        {
            realSolidDensityAverage = volVars.solidComponentDensity(cPhaseIdx);
        }

        if(realSolidDensityAverage <= volVars.solidComponentDensity(hPhaseIdx))
        {
            realSolidDensityAverage = volVars.solidComponentDensity(hPhaseIdx);
        }

        Scalar xH = volVars.solidVolumeFraction(hPhaseIdx)/0.227;

        Scalar qMass = 0.0;

         //discharge or hydration
        if (T < Teq){
            Scalar massFracH2O_fPhase = volVars.massFraction(0, H2OIdx);
            Scalar krh = getParam<Scalar>("Optimization.ReactionParam1");

            Scalar rHydration = - massFracH2O_fPhase* (- volVars.solidComponentDensity(hPhaseIdx)+ realSolidDensityAverage)
                                                     * krh * (T-Teq)/ Teq;
            Scalar n = getParam<Scalar>("Optimization.ReactionParam2");
            if((1-xH) < 0 + 1e-6) qMass = 0.0;
            else
                qMass =  rHydration*pow((1-xH), n);
        }

        //charge or dehydration
        else if(T > Teq){

            Scalar krd = 0.05;

            Scalar rDehydration = (volVars.solidComponentDensity(cPhaseIdx)- realSolidDensityAverage)
                                                     * krd * (Teq-T)/ Teq;

            qMass =  rDehydration;
        }

        if(Teq -T == 0) qMass = 0;

        return qMass;
    }

};

} // namespace Dumux

#endif
