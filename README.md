SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

G. Seitz, F. Mohammadi, H. Class<br>
Thermochemical heat storage in a lab-scale indirectly
operated CaO/Ca(OH)2 reactor - numerical modeling
and model validation through inverse parameter
estimation



Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir seitz2020a && cd seitz2020a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020a.git
```

After that, execute the file [installSeitz2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020a/-/blob/master/installSeitz2020a.sh).
```
chmod +x seitz2020a/installSeitz2020a.sh
./seitz2020a/installSeitz2020a.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run.

Applications
============

You can find all examples in the appl folder. To run them, navigate to:
```
cd seitz2020a/build-cmake/appl
```

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installSeitz2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/seitz2020a/-/blob/master/installSeitz2020a.sh).
